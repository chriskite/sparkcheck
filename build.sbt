organization := "com.chriskite"
organizationName := "Chris Kite"
startYear := Some(2018)
licenses += ("Apache-2.0", new URL("https://www.apache.org/licenses/LICENSE-2.0.txt"))

name := "sparkcheck"

version := "0.0.1"

scalaVersion := "2.11.11"

scalacOptions ++= Seq(
  "-language:postfixOps",
  "-Xfatal-warnings",
  "-unchecked",
  "-feature",
  "-deprecation"
)

libraryDependencies ++= Seq(
  "org.scalactic"  %% "scalactic"  % "3.0.5",
  "org.scalatest"  %% "scalatest"  % "3.0.5",
  "org.scalacheck" %% "scalacheck" % "1.14.0"
)

libraryDependencies ++= {
  val sparkVersion = "2.3.2"
  Seq(
    "spark-sql",
    "spark-mllib"
  ).map("org.apache.spark" %% _ % sparkVersion % Provided)
}

lazy val myProject = project
  .in(file("."))
  .enablePlugins(AutomateHeaderPlugin)

parallelExecution in Test := false
fork := true

publishTo := sonatypePublishTo.value
