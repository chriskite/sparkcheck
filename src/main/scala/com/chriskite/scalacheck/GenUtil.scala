/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chriskite.scalacheck
import org.scalacheck.Gen

private[scalacheck] object GenUtil {
  def sequence[T](gs: List[Gen[T]]): Gen[List[T]] =
    gs.foldRight(Gen.const[List[T]](List())) { (g: Gen[T], acc: Gen[List[T]]) =>
      acc.flatMap(lg => g.map(ge => lg :+ ge))
    }
}
