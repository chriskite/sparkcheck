/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
  * Includes code modified from holdenk/spark-testing-base
  */
package com.chriskite.scalacheck

import java.sql.{Date, Timestamp}

import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SQLContext}
import org.scalacheck.{Arbitrary, Gen}

import scala.collection.immutable

object DataFrameGen {
  def dataFrameGenerator(schema: StructType)(implicit sqlContext: SQLContext): Gen[DataFrame] = {
    arbitraryDataFrame(sqlContext, schema).arbitrary
  }

  def dataFrameGenerator(schema: StructType, columnGenerators: AbstractColumnGenerator*)(
      implicit sqlContext: SQLContext): Gen[DataFrame] = {
    arbitraryDataFrameWithCustomFields(sqlContext, schema)(columnGenerators: _*).arbitrary
  }

  /**
    * Creates a DataFrame Generator for the given Schema.
    *
    * @param sqlContext    SQL Context.
    * @param schema        The required Schema.
    * @param minPartitions minimum number of partitions, defaults to 1.
    * @return Arbitrary DataFrames generator of the required schema.
    */
  def arbitraryDataFrame(sqlContext: SQLContext, schema: StructType, minPartitions: Int = 1): Arbitrary[DataFrame] = {
    arbitraryDataFrameWithCustomFields(sqlContext, schema, minPartitions)()
  }

  /**
    * Creates a DataFrame Generator for the given Schema, and the given custom
    * generators.
    * Custom generators should be specified as a list of:
    * (column index, generator function) tuples.
    *
    * Note: The given custom generators should match the required schema,
    * for ex. you can't use Int generator for StringType.
    *
    * Note 2: The ColumnGenerator* accepted as userGenerators has changed.
    * ColumnGenerator is now the base class of the
    * accepted generators, users upgrading to 0.6 need to change their calls
    * to use Column.  Further explanation can be found in the release notes, and
    * in the class descriptions at the bottom of this file.
    *
    * @param sqlContext     SQL Context.
    * @param schema         The required Schema.
    * @param minPartitions  minimum number of partitions, defaults to 1.
    * @param userGenerators custom user generators in the form of:
    *                       (column index, generator function).
    *                       where column index starts from 0 to length - 1
    * @return Arbitrary DataFrames generator of the required schema.
    */
  def arbitraryDataFrameWithCustomFields(sqlContext: SQLContext, schema: StructType, minPartitions: Int = 1)(
      userGenerators: AbstractColumnGenerator*): Arbitrary[DataFrame] = {
    // ensure provided generators are all for columns that exist on the schema
    val fieldNames = schema.fields.map(_.name)
    userGenerators.map(_.columnName).foreach { ugName =>
      if (!fieldNames.contains(ugName)) throw NoSuchColumnError(ugName)
    }

    val idCols: immutable.Seq[String] =
      userGenerators.filter(_.isInstanceOf[IdColumnGenerator]).map(_.columnName).toList
    val arbitraryRDDs =
      RDDGen.genRDD(sqlContext.sparkContext, minPartitions)(getRowGenerator(schema, userGenerators))
    Arbitrary {
      arbitraryRDDs.map { rows =>
        sqlContext.createDataFrame(rows, schema).dropDuplicates(idCols)
      }
    }
  }

  /**
    * Creates row generator for the required schema.
    *
    * @param schema the required Row's schema.
    * @return Gen[Row]
    */
  def getRowGenerator(schema: StructType): Gen[Row] = {
    getRowGenerator(schema, List())
  }

  /**
    * Creates row generator for the required schema and with user's custom generators.
    *
    * Note: Custom generators should match the required schema, for example
    * you can't use Int generator for StringType.
    *
    * @param schema           the required Row's schema.
    * @param customGenerators user custom generator, this is useful if the you want
    *                         to control specific columns generation.
    * @return Gen[Row]
    */
  def getRowGenerator(schema: StructType, customGenerators: Seq[AbstractColumnGenerator]): Gen[Row] = {
    val generators: List[Gen[Any]] =
      createGenerators(schema.fields, customGenerators)
    val listGen: Gen[List[Any]] =
      Gen.sequence[List[Any], Any](generators)
    val generator: Gen[Row] =
      listGen.map(list => Row.fromSeq(list))
    generator
  }

  private def createGenerators(fields: Array[StructField],
                               userGenerators: Seq[AbstractColumnGenerator]): List[Gen[Any]] = {
    val generatorMap = userGenerators.map(generator => (generator.columnName -> generator)).toMap
    (0 until fields.length).toList.map { index =>
      generatorMap.get(fields(index).name) match {
        case None                                   => getGenerator(fields(index).dataType)
        case Some(gen: ColumnGenerator)             => gen.gen
        case Some(gen: IdColumnGenerator)           => getDataTypeGenerator(fields(index).dataType)
        case Some(structGen: StructColumnGenerator) => getGenerator(fields(index).dataType, structGen.gen)
      }
    }
  }

  private def getDataTypeGenerator(dataType: DataType): Gen[Any] = dataType match {
    case ByteType      => Arbitrary.arbitrary[Byte]
    case ShortType     => Arbitrary.arbitrary[Short]
    case IntegerType   => Arbitrary.arbitrary[Int]
    case LongType      => Arbitrary.arbitrary[Long]
    case FloatType     => Arbitrary.arbitrary[Float]
    case DoubleType    => Arbitrary.arbitrary[Double]
    case StringType    => Arbitrary.arbitrary[String]
    case BinaryType    => Arbitrary.arbitrary[Array[Byte]]
    case BooleanType   => Arbitrary.arbitrary[Boolean]
    case TimestampType => Arbitrary.arbLong.arbitrary.map(new Timestamp(_))
    case DateType      => Arbitrary.arbLong.arbitrary.map(new Date(_))
    case _             => throw new UnsupportedOperationException(s"Type: $dataType not supported")
  }

  private def getGenerator(dataType: DataType, generators: Seq[ColumnGenerator] = Seq()): Gen[Any] = dataType match {
    case arr: ArrayType => {
      val elementGenerator = getGenerator(arr.elementType)
      Gen.listOf(elementGenerator)
    }
    case map: MapType => {
      val keyGenerator   = getGenerator(map.keyType)
      val valueGenerator = getGenerator(map.valueType)
      val keyValueGenerator: Gen[(Any, Any)] = for {
        key   <- keyGenerator
        value <- valueGenerator
      } yield (key, value)

      Gen.mapOf(keyValueGenerator)
    }
    case row: StructType => getRowGenerator(row, generators)
    case _               => getDataTypeGenerator(dataType)
  }

}
