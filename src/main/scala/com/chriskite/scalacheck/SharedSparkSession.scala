/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chriskite.scalacheck

import org.apache.spark.SparkConf
import org.apache.spark.sql.internal.SQLConf
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, Suite}

/**
  * Helper trait for SQL test suites where all tests share a single SparkSession.
  */
trait SharedSparkSession extends BeforeAndAfterEach with BeforeAndAfterAll { self: Suite =>

  protected def sparkConf = {
    new SparkConf()
      .set("spark.unsafe.exceptionOnMemoryLeak", "true")
      .set("spark.ui.enabled", "false")
      .set(SQLConf.CODEGEN_FALLBACK.key, "false")
      .setMaster("local[*]")
      .setAppName("test")
  }

  /**
    * The SparkSession to use for all tests in this suite.
    *
    * By default, the underlying org.apache.spark.SparkContext will be run in local
    * mode with the default test configurations.
    */
  private var _spark: SparkSession = null

  /**
    * The SparkSession to use for all tests in this suite.
    */
  @transient implicit lazy val spark: SparkSession = _spark

  /**
    * The SQLContext to use for all tests in this suite.
    */
  @transient implicit lazy val sqlContext: SQLContext = _spark.sqlContext

  protected def createSparkSession: SparkSession = {
    cleanupAnyExistingSession()
    SparkSession.builder().config(sparkConf).getOrCreate()
  }

  protected def cleanupAnyExistingSession(): Unit = {
    val session = SparkSession.getActiveSession.orElse(SparkSession.getDefaultSession)
    if (session.isDefined) {
      session.get.stop()
      SparkSession.clearActiveSession()
      SparkSession.clearDefaultSession()
    }
  }

  /**
    * Initialize the SparkSession.  Generally, this is just called from
    * beforeAll; however, in test using styles other than FunSuite, there is
    * often code that relies on the session between test group constructs and
    * the actual tests, which may need this session.  It is purely a semantic
    * difference, but semantically, it makes more sense to call
    * 'initializeSession' between a 'describe' and an 'it' call than it does to
    * call 'beforeAll'.
    */
  protected def initializeSession(): Unit = {
    if (_spark == null) {
      _spark = createSparkSession
    }
  }

  /**
    * Make sure the SparkSession is initialized before any tests are run.
    */
  protected override def beforeAll(): Unit = {
    initializeSession()

    // Ensure we have initialized the context before calling parent code
    super.beforeAll()
  }

  /**
    * Stop the underlying org.apache.spark.SparkContext, if any.
    */
  protected override def afterAll(): Unit = {
    try {
      super.afterAll()
    } finally {
      try {
        if (_spark != null) {
          try {
            _spark.sessionState.catalog.reset()
          } finally {
            _spark.stop()
            _spark = null
          }
        }
      } finally {
        SparkSession.clearActiveSession()
        SparkSession.clearDefaultSession()
      }
    }
  }

  protected override def beforeEach(): Unit = {
    super.beforeEach()
  }

  protected override def afterEach(): Unit = {
    super.afterEach()
    // Clear all persistent datasets after each test
    spark.sharedState.cacheManager.clearCache()
  }
}
