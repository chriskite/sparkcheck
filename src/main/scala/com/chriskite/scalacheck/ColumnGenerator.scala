/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
  * Includes code modified from holdenk/spark-testing-base
  */
package com.chriskite.scalacheck

import org.scalacheck.{Arbitrary, Gen}

/**
  * Allows users to specify a generator for a specific column.
  */
object ColumnGenerator {
  def apply(columnName: String, generator: => Gen[Any]) = new ColumnGenerator(columnName, generator)
}

class ColumnGenerator(val columnName: String, generator: => Gen[Any]) extends AbstractColumnGenerator {
  lazy val gen = generator
}

/**
  * Allows users to specify an ID column that should be unique'd during DataFrame generation
  */
case class IdColumnGenerator(columnName: String) extends AbstractColumnGenerator

/**
  * Allows users to specify custom generators for a list of
  * columns inside a StructType column.
  */
object StructColumnGenerator {
  def apply(columnName: String, generators: => Seq[ColumnGenerator]) =
    new StructColumnGenerator(columnName, generators)
}

class StructColumnGenerator(val columnName: String, generators: => Seq[ColumnGenerator])
    extends AbstractColumnGenerator {
  lazy val gen = generators
}

/**
  * Abstract base for all column generator classes
  */
sealed abstract class AbstractColumnGenerator extends java.io.Serializable {
  val columnName: String
}

sealed trait ColumnGeneratorError
case class NoSuchColumnError(column: String)
    extends Exception(s"column generator column '$column' is not in schema")
    with ColumnGeneratorError
