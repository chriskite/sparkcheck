/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chriskite.scalacheck

import implicits._
import org.apache.spark.sql.SparkSession

class PartialOneToManyRelation(val fromDfName: Symbol, val toDfName: Symbol) extends PartialRelation {
  def onColumns(fromColName: String, toColName: String): OneToManyRelation = {
    new OneToManyRelation(fromDfName, toDfName, fromColName, toColName)
  }

}
class OneToManyRelation(val fromDfName: Symbol,
                        val toDfName: Symbol,
                        val fromColumnName: String,
                        val toColumnName: String)
    extends GeneratableRelation {
  def fillInRelation(dfMap: DataFrameMap)(implicit spark: SparkSession): DataFrameMap = {
    val fromDf = dfMap.get(fromDfName).get
    val toDf   = dfMap.get(toDfName).get
    val newToDf = toDf.schema(toColumnName).dataType.typeName match {
      case "long"   => fromDf.randomlyOneToMany[Long](toDf, fromColumnName, toColumnName)
      case "string" => fromDf.randomlyOneToMany[String](toDf, fromColumnName, toColumnName)
      case _        => throw new Exception("unsupported type for randomlyManyToOne")
    }

    dfMap.updated(toDfName, newToDf)
  }
}

class PartialManyToOneRelation(val fromDfName: Symbol, val toDfName: Symbol) extends PartialRelation {
  def onColumns(fromColName: String, toColName: String): ManyToOneRelation = {
    new ManyToOneRelation(fromDfName, toDfName, fromColName, toColName)
  }
}

class ManyToOneRelation(val fromDfName: Symbol,
                        val toDfName: Symbol,
                        val fromColumnName: String,
                        val toColumnName: String)
    extends GeneratableRelation {
  def fillInRelation(dfMap: DataFrameMap)(implicit spark: SparkSession): DataFrameMap = {
    val fromDf = dfMap.get(fromDfName).get
    val toDf   = dfMap.get(toDfName).get
    val newFromDf = fromDf.schema(fromColumnName).dataType.typeName match {
      case "long"   => fromDf.randomlyManyToOne[Long](toDf, fromColumnName, toColumnName)
      case "string" => fromDf.randomlyManyToOne[String](toDf, fromColumnName, toColumnName)
      case _        => throw new Exception("unsupported type for randomlyManyToOne")
    }

    dfMap.updated(fromDfName, newFromDf)
  }

}

trait GeneratableRelation {
  def fillInRelation(dfMap: DataFrameMap)(implicit spark: SparkSession): DataFrameMap
}

trait PartialRelation {
  def onColumns(fromColName: String, toColName: String): GeneratableRelation
}
