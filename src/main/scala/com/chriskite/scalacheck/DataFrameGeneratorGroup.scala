/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chriskite.scalacheck

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.scalacheck.Gen

case class NamedDataFrame(name: Symbol, df: DataFrame)

class DataFrameGeneratorGroup(generators: List[Gen[NamedDataFrame]], relations: List[GeneratableRelation]) {
  protected val _generators: List[Gen[NamedDataFrame]] = generators
  protected val _relations: List[GeneratableRelation]  = relations

  def withGenerators(gens: Map[Symbol, Gen[DataFrame]]): DataFrameGeneratorGroup = {
    val genList: List[Gen[NamedDataFrame]] = gens.map {
      case (name, genDf) =>
        genDf.map(NamedDataFrame(name, _))
    }.toList
    new DataFrameGeneratorGroup(genList, _relations)
  }

  def withRelations(rels: GeneratableRelation*): DataFrameGeneratorGroup = {
    new DataFrameGeneratorGroup(_generators, rels.toList)
  }

  def generator(implicit spark: SparkSession): Gen[DataFrameGroup] = GenUtil.sequence(_generators).map { namedDfs =>
    val dfMap = namedDfs.map(namedDf => (namedDf.name, namedDf.df)).toMap
    val newDfMap = _relations.foldLeft(dfMap) { (dfMapAcc, rel) =>
      rel.fillInRelation(dfMapAcc)
    }
    new DataFrameGroup(newDfMap)
  }
}

object DataFrameGeneratorGroup {
  def apply() = new DataFrameGeneratorGroup(List(), List())
}

class DataFrameGroup(val dfMap: DataFrameMap) {
  def get(dfName: Symbol): Option[DataFrame] = dfMap.get(dfName)
  def show: Unit =
    dfMap.map {
      case (name, df) =>
        println(s"$name:")
        df.show
    }
}
