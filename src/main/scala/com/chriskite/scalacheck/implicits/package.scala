/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chriskite.scalacheck
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

import scala.reflect.ClassTag
import scala.reflect.runtime.universe.TypeTag

package object implicits {
  implicit class DataFrameExt(df: DataFrame) {
    protected val otherIdxCol: String  = "_other_idx"
    protected val randomIdxCol: String = "_random_idx"
    def randomlyManyToOne[T: TypeTag: ClassTag](otherDf: DataFrame, fromColumn: String, toColumn: String)(
        implicit spark: SparkSession): DataFrame = {
      val otherDfIndexed = spark
        .createDataFrame(otherDf.select(toColumn).rdd.map(_.getAs[T](0)).zipWithIndex)
        .withColumnRenamed("_1", fromColumn)
        .withColumnRenamed("_2", otherIdxCol)
      val dfIndexed = df.withColumn(randomIdxCol, (rand() * otherDf.count).cast(IntegerType))

      dfIndexed
        .drop(fromColumn)
        .join(otherDfIndexed, col(randomIdxCol) === col(otherIdxCol))
        .drop(otherIdxCol)
        .drop(randomIdxCol)
        .filter(col(fromColumn) isNotNull)
    }

    def randomlyOneToMany[T: TypeTag: ClassTag](otherDf: DataFrame, fromColumn: String, toColumn: String)(
        implicit spark: SparkSession): DataFrame = {
      otherDf.randomlyManyToOne[T](df, toColumn, fromColumn)
    }
  }

  implicit class PartialRelationSymbol(dfName: Symbol) {
    def oneToMany(toDfName: Symbol): PartialRelation = new PartialOneToManyRelation(dfName, toDfName)
    def manyToOne(toDfName: Symbol): PartialRelation = new PartialManyToOneRelation(dfName, toDfName)
  }
}
