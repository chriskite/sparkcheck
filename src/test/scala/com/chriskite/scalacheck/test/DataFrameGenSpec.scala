/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chriskite.scalacheck.test

import com.chriskite.scalacheck.DataFrameGen._
import com.chriskite.scalacheck._
import com.chriskite.scalacheck.implicits._
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.scalacheck.Gen
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{Matchers, WordSpec}

class DataFrameGenSpec extends WordSpec with SharedSparkSession with Matchers with GeneratorDrivenPropertyChecks {
  import data.Schemas._
  import spark.implicits._

  "DataFrameGen" should {
    "generate a DataFrame with the specified schema" in {
      val dfGen = dataFrameGenerator(userSchema)
      forAll(dfGen) { df =>
        df.schema shouldBe userSchema
      }
    }
    "raise an error when a column generator is provided that doesn't match the column name in the schema" in {
      val column = "non_existent_column"
      val msg    = s"column generator column '$column' is not in schema"
      val badGen = ColumnGenerator(column, Gen.posNum[Long])
      the[NoSuchColumnError] thrownBy dataFrameGenerator(userSchema, badGen) should have message msg
    }
    "generate a DataFrame with generator columns" in {
      val idGen   = ColumnGenerator("user_id", Gen.posNum[Long])
      val nameGen = ColumnGenerator("name", Gen.alphaStr suchThat (_.nonEmpty))
      val dfGen   = dataFrameGenerator(userSchema, idGen, nameGen)
      forAll(dfGen) { df =>
        val rows             = df.collect
        val ids: Array[Long] = rows.map(_.getLong(0))
        val names            = rows.map(_.getString(1))

        df.schema shouldBe userSchema
        all(ids) should be > (0L)
        all(names) should not be empty
      }
    }
    "generate a DataFrame with a distinct Long ID column" in {
      val dfGen = dataFrameGenerator(userSchema, IdColumnGenerator("user_id"))
      forAll(dfGen) { df =>
        val rows             = df.collect
        val ids: Array[Long] = rows.map(_.getLong(0))

        df.schema shouldBe userSchema
        all(ids) shouldBe a[java.lang.Long]
        ids shouldBe ids.distinct
      }
    }
    "generate a DataFrame with a distinct String ID column" in {
      val schema = StructType(
        List(
          StructField("uuid", StringType, false),
          StructField("name", StringType, true)
        ))
      val dfGen = dataFrameGenerator(schema, IdColumnGenerator("uuid"))
      forAll(dfGen) { df =>
        val rows               = df.collect
        val ids: Array[String] = rows.map(_.getString(0))

        df.schema shouldBe schema
        all(ids) shouldBe a[String]
        ids shouldBe ids.distinct
      }
    }
    "generate and check related DataFrames" when {
      "one relation on the df" in {
        val dfGenGroup = DataFrameGeneratorGroup()
          .withGenerators(
            Map(
              'users -> dataFrameGenerator(userSchema, IdColumnGenerator("user_id")),
              'posts -> dataFrameGenerator(postSchema, IdColumnGenerator("post_id"))
            ))
          .withRelations(
            'users oneToMany 'posts onColumns ("user_id", "user_id")
          )

        forAll(dfGenGroup.generator) { group: DataFrameGroup =>
          for {
            usersDf <- group.get('users)
            postsDf <- group.get('posts)
          } yield {
            val postsWithUsers = postsDf.join(usersDf, Seq("user_id"), "inner")
            postsWithUsers.count shouldBe postsDf.count
          }
        }
      }
      "mulitple relations on the df" in {
        val dfGenGroup = DataFrameGeneratorGroup()
          .withGenerators(Map(
            'users    -> dataFrameGenerator(userSchema, IdColumnGenerator("user_id")),
            'posts    -> dataFrameGenerator(postSchema, IdColumnGenerator("post_id")),
            'comments -> dataFrameGenerator(commentSchema, IdColumnGenerator("commentId"))
          ))
          .withRelations(
            'users oneToMany 'posts onColumns ("user_id", "user_id"),
            'comments manyToOne 'users onColumns ("userId", "user_id"),
            'comments manyToOne 'posts onColumns ("postId", "post_id")
          )

        forAll(dfGenGroup.generator) { group: DataFrameGroup =>
          for {
            usersDf    <- group.get('users)
            postsDf    <- group.get('posts)
            commentsDf <- group.get('comments)
          } yield {
            commentsDf.schema shouldBe commentSchema

            commentsDf.filter($"postId" isNull).count shouldBe 0
            commentsDf.filter($"userId" isNull).count shouldBe 0

            val userIds        = usersDf.select("user_id").collect.map(_.getLong(0))
            val postIds        = postsDf.select("post_id").collect.map(_.getLong(0))
            val commentUserIds = commentsDf.select("userId").collect.map(_.getLong(0))
            val commentPostIds = commentsDf.select("postId").collect.map(_.getLong(0))
            commentUserIds.foreach(id => assert(userIds.contains(id)))
            commentPostIds.foreach(id => assert(postIds.contains(id)))
          }
        }
      }
    }
  }
}
