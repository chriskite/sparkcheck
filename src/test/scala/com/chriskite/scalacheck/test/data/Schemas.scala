/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chriskite.scalacheck.test.data
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}

object Schemas {
  val userSchema = StructType(
    List(
      StructField("user_id", LongType, false),
      StructField("name", StringType, true)
    ))
  val postSchema = StructType(
    List(
      StructField("post_id", LongType, false),
      StructField("title", StringType, true),
      StructField("user_id", LongType, false)
    ))
  val commentSchema = StructType(
    List(
      StructField("commentId", LongType, false),
      StructField("content", StringType, true),
      StructField("userId", LongType, false),
      StructField("postId", LongType, false)
    ))
}
