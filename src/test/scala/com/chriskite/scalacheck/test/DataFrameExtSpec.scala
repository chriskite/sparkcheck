/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chriskite.scalacheck.test

import com.chriskite.scalacheck.DataFrameGen._
import com.chriskite.scalacheck.{IdColumnGenerator, SharedSparkSession}
import com.chriskite.scalacheck.implicits._
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{Matchers, WordSpec}

class DataFrameExtSpec extends WordSpec with SharedSparkSession with Matchers with GeneratorDrivenPropertyChecks {
  import data.Schemas._
  import spark.implicits._

  "randomlyManyToOne" when {
    "the foreign df is empty" should {
      "return an empty df with the correct schema" in {
        val postsGen = dataFrameGenerator(postSchema, IdColumnGenerator("post_id"))
        // generate an empty users df
        val usersGen = dataFrameGenerator(userSchema, IdColumnGenerator("user_id")).map(_.filter(_ => false))

        forAll(postsGen, usersGen) { (postsDf, usersDf) =>
          val newPostsDf = postsDf.randomlyManyToOne[Long](usersDf, "user_id", "user_id")
          newPostsDf.schema shouldBe postSchema
          newPostsDf.count shouldBe 0
        }
      }
    }
    "the foreign df is not empty" should {
      "assign all rows in the df an id from the foreign df" in {
        val postsGen = dataFrameGenerator(postSchema, IdColumnGenerator("post_id"))
        val usersGen = dataFrameGenerator(userSchema, IdColumnGenerator("user_id"))

        forAll(postsGen, usersGen) { (postsDf, usersDf) =>
          val newPostsDf = postsDf.randomlyManyToOne[Long](usersDf, "user_id", "user_id")
          val joinedDf   = newPostsDf.join(usersDf, Seq("user_id"), "inner")
          newPostsDf.schema shouldBe postSchema
          joinedDf.count shouldBe postsDf.count
          newPostsDf.filter($"user_id" isNull).count shouldBe 0
          val userIds: List[Long] = usersDf.select("user_id").collect.map(_.getLong(0)).toList
          val postUserIds         = joinedDf.select("user_id").collect.map(_.getLong(0)).toList
          postUserIds.foreach(id => assert(userIds.contains(id)))
        }
      }
      "work on a df that has already been randomlyManyToOne'd" in {
        val postsGen    = dataFrameGenerator(postSchema, IdColumnGenerator("post_id"))
        val usersGen    = dataFrameGenerator(userSchema, IdColumnGenerator("user_id"))
        val commentsGen = dataFrameGenerator(commentSchema, IdColumnGenerator("commentId"))

        forAll(postsGen, usersGen, commentsGen) { (postsDf, usersDf, commentsDf) =>
          val relatedCommentsDf =
            commentsDf
              .randomlyManyToOne[Long](usersDf, "userId", "user_id")
              .randomlyManyToOne[Long](postsDf, "postId", "post_id")

          relatedCommentsDf.filter($"userId" isNull).count shouldBe 0
          relatedCommentsDf.filter($"postId" isNull).count shouldBe 0

          val userIds: List[Long]        = usersDf.select("user_id").collect.map(_.getLong(0)).toList
          val postIds: List[Long]        = postsDf.select("post_id").collect.map(_.getLong(0)).toList
          val commentUserIds: List[Long] = relatedCommentsDf.select("userId").collect.map(_.getLong(0)).toList
          val commentPostIds: List[Long] = relatedCommentsDf.select("postId").collect.map(_.getLong(0)).toList
          commentUserIds.foreach(id => assert(userIds.contains(id)))
          commentPostIds.foreach(id => assert(postIds.contains(id)))
        }
      }
    }
  }
}
