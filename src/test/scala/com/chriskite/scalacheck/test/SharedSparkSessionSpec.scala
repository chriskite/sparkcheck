/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chriskite.scalacheck.test

import com.chriskite.scalacheck.SharedSparkSession
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.scalatest.{Matchers, WordSpec}

class SharedSparkSessionSpec extends WordSpec with SharedSparkSession with Matchers {
  "SharedSparkSession" should {
    "provide a spark session for the test suite" in {
      spark shouldBe a[SparkSession]
    }
    "provide a sql context for the test suite" in {
      sqlContext shouldBe a[SQLContext]
    }
    "create DataFrames" in {
      import spark.implicits._
      val nums: List[Int]   = List(1, 2, 3)
      val result: List[Int] = nums.toDF("num").collect().map(_.getInt(0)).toList
      assert(nums === result)
    }
  }
}
