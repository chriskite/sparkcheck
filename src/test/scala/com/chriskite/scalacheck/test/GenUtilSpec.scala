/*
 * Copyright 2018 Chris Kite
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chriskite.scalacheck.test
import com.chriskite.scalacheck.GenUtil
import org.scalacheck.Gen
import org.scalatest.{Matchers, WordSpec}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

class GenUtilSpec extends WordSpec with Matchers with GeneratorDrivenPropertyChecks {
  "GenUtil.sequence" when {
    "list is empty" should {
      "return a generator of an empty list" in {
        val gen: Gen[List[Nothing]] = GenUtil.sequence(List())
        forAll(gen) { list =>
          list shouldBe empty
        }
      }
    }
    "list is non-empty" should {
      "return a generator of a list of T" in {
        val gen: Gen[List[Int]] = GenUtil.sequence(List(Gen.posNum[Int], Gen.posNum[Int]))
        forAll(gen) { list =>
          list should have size 2
          all(list) should be > 0
        }
      }
    }
  }
}
